package merchan.facci.gestiondefacturas;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView mBatteryLevelText;
    private ProgressBar mBatteryLevelProgress;



    @Override
    protected void onStart() {
        super.onStart();
        Log.e( "4A", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("4A","onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("4A","onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e ("4A", "onRestart");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("4A","onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("4A","onDestroy");
    }


    private class BaterryBroadcastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL,0);
            mBatteryLevelText.setText(level + " " + getString(R.string.nivel_de_bateria));
            mBatteryLevelProgress.setProgress(level);


        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.e("4A","OnCreate");

        mBatteryLevelText = (TextView) findViewById(R.id.textView);
        mBatteryLevelProgress = (ProgressBar)findViewById(R.id.progressBar);


    }





}
